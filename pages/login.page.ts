import { Page } from '@playwright/test';
import { SideMenuComponent } from '../components/side-menu.component';

export class LoginPage {
  constructor(private page: Page) {}

  loginImput = this.page.getByTestId('login-input');
  passwordInput = this.page.getByTestId('password-input');
  loginButton = this.page.getByTestId('login-button');

  loginError = this.page.getByTestId('error-login-id');
  passwordError = this.page.getByTestId('error-login-password');

  async login(userId: string, userPassword: string): Promise<void> {
    await this.loginImput.fill(userId);
    await this.passwordInput.fill(userPassword);
    await this.loginButton.click();
  }
}
