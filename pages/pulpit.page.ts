import { Page } from '@playwright/test';
import { SideMenuComponent } from '../components/side-menu.component';

export class PulpitPage {
  constructor(private page: Page) {}

  sideMenu = new SideMenuComponent(this.page);

  expectedUserName = this.page.getByTestId('user-name');
  receiverId = this.page.locator('#widget_1_transfer_receiver');
  transferAmount = this.page.locator('#widget_1_transfer_amount');
  transferTitle = this.page.locator('#widget_1_transfer_title');

  executeButtn = this.page.locator('#execute_btn');
  closeButton = this.page.getByTestId('close-button');

  expectedMessages = this.page.locator('#show_messages');

  receiverPhone = this.page.locator('#widget_1_topup_receiver');
  amount = this.page.locator('#widget_1_topup_amount');
  popUpAgreement = this.page.locator('#uniform-widget_1_topup_agreement span');

  phoneButton = this.page.locator('#execute_phone_btn');
  moneyValue = this.page.locator('#money_value');

  async executeQuickPayment(
    receiverId: string,
    transferAmount: string,
    transferTitle: string,
  ): Promise<void> {
    await this.receiverId.selectOption(receiverId);
    await this.transferAmount.fill(transferAmount);
    await this.transferTitle.fill(transferTitle);
    await this.executeButtn.click();
    await this.closeButton.click();
  }

  async executeMobileTopUp(receiverPhone: string, amount: string): Promise<void> {
    await this.receiverPhone.selectOption(receiverPhone);
    await this.amount.fill(amount);
    await this.popUpAgreement.click();
    await this.phoneButton.click();
    await this.closeButton.click();
  }
}
