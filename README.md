# Demo-Bank website tests

## Links:

https://demo-bank.vercel.app

## Commands:

- check `NodeJS` version

`node -v`

- new project with Playwright

`npm init playwright@latest`

-record test for given site

`npx playwright codegen [website address]`

- run tests without browser GUI

'npx playwright test`

- run tests with browser GUI

'npx playwright test --headed`

- view report

`npx playwright show-report`

- run Prettier

`npx prettier --write .`
