export const loginData = {
  userId: 'test1234',
  incorrectUserId: 'test',
  passwordId: '12345678',
  incorrectUserPassword: '1234567',
  expectedLoginError: 'identyfikator ma min. 8 znaków',
  expectedPasswordError: 'hasło ma min. 8 znaków',
};
