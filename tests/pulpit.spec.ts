import { test, expect } from '@playwright/test';
import { loginData } from '../test-data/login.data';
import { LoginPage } from '../pages/login.page';
import { pulpitData } from '../test-data/pulpit.data';
import { PulpitPage } from '../pages/pulpit.page';

test.describe('Pulpit tests', () => {
  let pulpitPage: PulpitPage;

  test.beforeEach(async ({ page }) => {
    pulpitPage = new PulpitPage(page);

    const userId = loginData.userId;
    const userPassword = loginData.passwordId;

    await page.goto('/');

    const loginPage = new LoginPage(page);
    await loginPage.login(userId, userPassword);
  });

  test('quick payment with correct data', async ({ page }) => {
    //Arrange
    const receiverId = pulpitData.receiverId;
    const transferAmount = pulpitData.transferAmount;
    const transferTitle = pulpitData.transferTitle;
    const expectedTransferReceiver = pulpitData.expectedUserName;

    //Act
    await pulpitPage.executeQuickPayment(receiverId, transferAmount, transferTitle);

    //Assert
    await expect(pulpitPage.expectedMessages).toHaveText(
      `Przelew wykonany! ${expectedTransferReceiver} - ${transferAmount},00PLN - ${transferTitle}`,
    );
  });

  test('successful mobile top-up', async ({ page }) => {
    //Arrange
    const receiverPhone = pulpitData.receiverPhone;
    const amount = pulpitData.amount;

    //Act
    await pulpitPage.executeMobileTopUp(receiverPhone, amount)

    //Assert
    await expect(pulpitPage.expectedMessages).toHaveText(
      `Doładowanie wykonane! ${amount},00PLN na numer  ${receiverPhone}`,
    );
  });

  test('correct balance after successful mobile top-up', async ({ page }) => {
    //Arrange
    const receiverPhone = pulpitData.receiverPhone;
    const amount = pulpitData.amount;
    const initialBalance = await pulpitPage.moneyValue.innerText();
    const expectedBalance = Number(initialBalance) - Number(amount);

    //Act
    await pulpitPage.executeMobileTopUp(receiverPhone, amount)

    //Assert
    await expect(pulpitPage.moneyValue).toHaveText(`${expectedBalance}`);
  });
});
